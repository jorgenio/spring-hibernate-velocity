--
-- PostgreSQL database dump
--

-- Dumped from database version 9.0.2
-- Dumped by pg_dump version 9.0.2
-- Started on 2012-11-25 21:17:02 BOT

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- TOC entry 1785 (class 1262 OID 17236)
-- Name: events; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE events WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'es_BO.UTF-8' LC_CTYPE = 'es_BO.UTF-8';


\connect events

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- TOC entry 309 (class 2612 OID 11574)
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: -
--

CREATE OR REPLACE PROCEDURAL LANGUAGE plpgsql;


SET search_path = public, pg_catalog;

SET default_with_oids = false;

--
-- TOC entry 1501 (class 1259 OID 17237)
-- Dependencies: 5
-- Name: events; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE events (
    etitle character varying,
    edate date,
    eid bigint NOT NULL
);


--
-- TOC entry 1502 (class 1259 OID 17254)
-- Dependencies: 5 1501
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 1787 (class 0 OID 0)
-- Dependencies: 1502
-- Name: hibernate_sequence; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE hibernate_sequence OWNED BY events.eid;


--
-- TOC entry 1780 (class 2604 OID 17256)
-- Dependencies: 1502 1501
-- Name: eid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE events ALTER COLUMN eid SET DEFAULT nextval('hibernate_sequence'::regclass);


--
-- TOC entry 1782 (class 2606 OID 17264)
-- Dependencies: 1501 1501
-- Name: eid; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY events
    ADD CONSTRAINT eid PRIMARY KEY (eid);


-- Completed on 2012-11-25 21:17:03 BOT

--
-- PostgreSQL database dump complete
--

